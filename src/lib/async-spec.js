// Jasmine.Async, v0.1.0
// Copyright (c)2012 Muted Solutions, LLC. All Rights Reserved.
// Distributed under MIT license
// http://github.com/derickbailey/jasmine.async

module.exports = function runAsync(block){
  return function(){
    var done = false;
    var complete = function(){ done = true; };

    runs(function(){
      block(complete);
    });

    waitsFor(function(){
      return done;
    });
  };
}

// module.exports = function () {
//   class AsyncSpec {
//     constructor(spec) {
//       this.spec = spec;
//     }
  
//     beforeEach(block){
//       this.spec.beforeEach(runAsync(block));
//     }
  
//     afterEach(block){
//       this.spec.afterEach(runAsync(block));
//     }
  
//     it(g, description, block){
//       // For some reason, `it` is not attached to the current
//       // test suite, so it has to be called from the global
//       // context.
//       g(description, runAsync(block));
//     }
//   }

//   return AsyncSpec
// }