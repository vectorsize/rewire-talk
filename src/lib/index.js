const fetch = require('node-fetch')

module.exports = {
  thunk: action => next => action.then(next),
  API: {
    get: r => {
      switch(r) {
        case'countries':
          return fetch('https://gist.githubusercontent.com/vectorsize/210568d30199a614f6b97be7ec5fa0c0/raw/f40ab0a645206e642dd10d5c328b80053358d57b/countries.json')
            .then(d => d.json())
            .then(d => d[1])
          }
        }
      }
}