import React, { Component } from 'react'
import { toPairs, last, prop, mergeDeepRight as merge } from 'ramda'
import cx from 'classnames'

import countries from './data/countries'
import styles from './app.module.css'

const getDropdown = prop('dropdown')

class App extends Component {
  
  state = {
    dropdown: {
      label: 'Countries',
      options: countries[1],
      selected: -1,
      open: false
    }
  }

  componentDidMount() {
    this.setLabel(getDropdown(this.state).selected)
  }

  openDropdown = () => {
    this.setState(merge(this.state, { dropdown: { open: true } }))
  }

  closeDropdown = () => {
    this.setState(merge(this.state, { dropdown: { open: false } }))
  }

  toggleDropdown = () => {
    const isOpen = getDropdown(this.state).open
    this.setState(merge(this.state, { dropdown: { open: !isOpen } }))
  }

  selectDropdown = selected => {
    const label = this.getLabel(selected)
    this.setState(merge(this.state, { dropdown: { selected, open: false, label } }))
  }

  setLabel = selected => {
    const dropdown = getDropdown(this.state)
    const label = this.getLabel(selected) || this.getLabel(dropdown.selected) || dropdown.label
    this.setState(merge(this.state, { dropdown: { label } }))
  }

  getLabel = sel => {
    const options = getDropdown(this.state).options
    return options[sel] && options[sel].name
  }

  globalClick = e => last(toPairs(e.target.dataset)) && last(toPairs(e.target.dataset)).includes('dropdown')
      ? false
      : this.setState(merge(this.state, { dropdown: { open: false } }))

  render() {
    const { dropdown: { options, selected, open, label }} = this.state
    const { toggleDropdown, selectDropdown, openDropdown, globalClick } = this
    return (
      <div className={styles.app} onClick={globalClick}>
        <div data-dropdown className={styles.dropdown}>
          <div data-dropdown className={styles.container}>
            <h1
              data-dropdown
              className={cx(styles.label, {[styles.labelOpen]: open})}
              onClick={toggleDropdown}>{label}
            </h1>
            { open &&
              <ul data-dropdown className={styles.items}>{ options.map((option, key) => {
                return (
                  <li
                    data-dropdown
                    className={cx(styles.item, {[styles.itemSelected]: selected === key}) }
                    key={key}
                    onClick={() => console.log(key) || selectDropdown(key)}>{option.name}</li>
                )
              })}
            </ul>
          }
          </div>
        </div>
        <div className={styles.info}>
          <span className={styles.infoIcon}>I</span>
          To get started please <button
              data-dropdown
              className={styles.infoButton}
              onMouseEnter={openDropdown}
            >choose a country.</button>
        </div>
      </div>
    )
  }
}

export default App
